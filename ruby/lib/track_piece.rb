class TrackPiece

  PI = 3.14159

  attr_reader :length
  attr_reader :radius
  attr_reader :angle

  def initialize(pieceData)
    @length = pieceData.has_key?('length') ? pieceData['length'] : 0
    @radius = pieceData.has_key?('radius') ? pieceData['radius'] : 0
    @angle = pieceData['angle'] if pieceData.has_key?('angle')
    @hasSwitch = pieceData['switch'] if pieceData.has_key?('switch')
  end

  def has_switch?
    @hasSwitch
  end

  def lane?()
    @length > 0
  end

  def curve?()
    @radius > 0
  end

  # Determine which turn - left or right depending on angle
  def get_turn
    if !@angle.nil?
      @angle > 0 ? 'Right' : 'Left'
    end
  end

  # Determines how long distance passed
  def percentPassed(aCar)
    (aCar.in_piece_distance / @length) unless @length == 0
  end

  def getCurveLength
    PI * @radius * @angle / 180
  end
end
