require './lib/track.rb'

class Car

  DefaultThrottle = 0.50

  TrackSpeedList = {
      :keimola => {
        'DefaultThrottle' => 0.60,
        'FullThrottle' => 0.8,
        'Curve45Throttle' => 0.5,
        'Curve22Throttle' => 0.65,
        'Radius200Throttle' => 0.65,
        'Radius100Throttle' => 0.52,
        'Radius50Throttle' => 0.3
      },
      :germany => {
        'DefaultThrottle' => 0.50,
        'FullThrottle' => 0.65,
        'Curve45Throttle' => 0.5,
        'Curve22Throttle' => 0.65,
        'Radius200Throttle' => 0.5,
        'Radius100Throttle' => 0.4,
        'Radius50Throttle' => 0.3
      },
      :usa => {

      }
  }

  #DefaultThrottle = 0.50
  ## FullThrottle = 0.8
  #FullThrottle = 0.65 # Works for Germany!
  #Curve45Throttle = 0.5
  #Curve22Throttle = 0.65
  #Radius200Throttle = 0.5
  #Radius100Throttle = 0.4
  #Radius50Throttle = 0.3

  attr_accessor :in_piece_distance
  attr_reader :angle
  attr_reader :piece_index
  attr_reader :startLaneIdx
  attr_reader :endLaneIdx
  attr_reader :name

  def initialize(carData)
    @color = carData['color']
    @name = carData['name']
    @track_speed_list = TrackSpeedList
  end

  # Determine throttle basing on current situation and overall Track details
  def get_throttle(piece_index, track_piece, aTrack)

    # Get list of speeds for current Track
    track_speeds = @track_speed_list[aTrack.id.to_sym]

    throttle_value =
      if track_piece.lane?
        if aTrack.nextPieceHasTurn?(piece_index)
          # FIXME: Find out how sharp next turn is and slow down respectively
          if track_piece.percentPassed(self) >= 0.65 and track_piece.percentPassed(self) < 0.8
            track_speeds['FullThrottle'] * 0.9
          elsif track_piece.percentPassed(self) >= 0.8
            track_speeds['FullThrottle'] * 0.65
          else
            track_speeds['FullThrottle']
          end
        else
          track_speeds['FullThrottle']
        end
      else
        # FIXME: Throttle should respond to sharpness of the curve
        if track_piece.radius > 90 and track_piece.radius < 180
          track_speeds['Radius100Throttle']
        elsif track_piece.radius < 90
          track_speeds['Radius50Throttle']
        elsif track_piece.radius >= 180
          track_speeds['Radius200Throttle']
        end
        # if track_piece.angle >= 22 and track_piece.angle < 45
        #   track_speeds['Curve22Throttle']
        # else
        #   track_speeds['Curve45Throttle']
        # end
      end
  end

  def switch_lane(direction)
    # puts "Switching lane to the " << direction
    JSON.generate({:msgType => 'switchLane', :data => direction})
  end

  def throttle(value)
    # puts "Throttle value: #{value}"
    JSON.generate({:msgType => 'throttle', :data => value})
  end

  # Either throttle or switch lane
  def send_message(data, aTrack)
    piece_index = data['piecePosition']['pieceIndex']
    self.in_piece_distance = data['piecePosition']['inPieceDistance']

    track_piece = aTrack.get_piece(piece_index)

    if track_piece.has_switch? && !aTrack.get_piece(piece_index + 1).nil?
      # Take closer to inner radius
      # TODO: Maybe on some turns it makes more sense to take outer radius?
      switch_to = aTrack.get_piece(piece_index + 1).get_turn
    end

    # if !switch_to.nil?
    #   switch_lane(switch_to)
    # else
      throttle(get_throttle(piece_index, track_piece, aTrack) || DefaultThrottle)
    # end
  end
end