require './lib/track_piece.rb'

class Track
  attr_reader :id
  attr_reader :name

  def initialize(trackData)
    @id = trackData['id']
    @name = trackData['name']
    @pieces = [] # Keeps pieces of track
  end

  def add_piece(aPiece)
    @pieces << aPiece
  end

  def get_piece(idx)
    @pieces[idx]
  end

  def nextPieceHasTurn?(idx)
    if !@pieces[idx + 1].nil?
      @pieces[idx + 1].curve?
    end
  end
end
